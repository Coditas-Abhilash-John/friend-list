import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})

export class ButtonComponent implements OnInit {
  @Input() showAddButton: boolean = false;
  buttonType: string = '';
  constructor() {}
  ngOnInit(): void {
    if (this.showAddButton) this.buttonType = 'person_add';
    else this.buttonType = 'person_remove';
  }
}
