import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPeople } from 'src/app/app.types';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @Input() peopleList: IPeople[] = [];
  constructor() {}

  @Output() changeFriendshipStatus = new EventEmitter<number>();

  ngOnInit(): void {}

  onFriendshipChange(personId: number) {
    this.changeFriendshipStatus.emit(personId);
  }
}
