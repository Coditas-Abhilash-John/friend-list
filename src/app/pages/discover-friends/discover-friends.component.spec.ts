import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoverFriendsComponent } from './discover-friends.component';

describe('DiscoverFriendsComponent', () => {
  let component: DiscoverFriendsComponent;
  let fixture: ComponentFixture<DiscoverFriendsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiscoverFriendsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DiscoverFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
