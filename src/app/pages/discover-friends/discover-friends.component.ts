import { Component, OnInit } from '@angular/core';
import { IPeople } from 'src/app/app.types';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-discover-friends',
  templateUrl: './discover-friends.component.html',
  styleUrls: ['./discover-friends.component.scss'],
})
export class DiscoverFriendsComponent implements OnInit {
  peopleList: IPeople[] = [];

  constructor(private httpService: HttpService) {}

  async ngOnInit (){
    this.peopleList= await this.httpService.getPeopleList();
  }

getPeopleWithFriendshipStatus(hasFriendship: boolean) {
    return this.peopleList.filter(
      (people) => people.isFriend === hasFriendship
    );
  }

onFriendshipStatusChange(personId: number) {
    const personIndex = this.peopleList.findIndex((p) => p.id === personId);
    if (personIndex === -1) {
      return;
    }
    this.peopleList[personIndex].isFriend = !this.peopleList[personIndex].isFriend;
  }
}
