import { ReturnStatement } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { backendPeopleList } from 'src/mock/backend';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor() {}
  async getPeopleList() {
    const peopleListData = await backendPeopleList();
    return peopleListData;
  }
}
