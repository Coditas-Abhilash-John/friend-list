import { IPeople } from 'src/app/app.types';

export const backendPeopleList = async (): Promise<IPeople[]> => {
  return [
    { id: 1, name: 'Himmler', isFriend: false },
    { id: 2, name: 'Yamoto', isFriend: false },
    { id: 3, name: 'Churchill', isFriend: false },
    { id: 4, name: 'Franklin', isFriend: false },
    { id: 5, name: 'Adolf', isFriend: false },
  ];
};
